# Welcome to the Tezos Agora Wiki!

The Tezos Agora Wiki is a place to get started and learn about Tezos. It also aims to answer the frequently asked questions about the Tezos protocol & the Tezos ecosystem.

This wiki aspires to be a living document that will evolve as the Tezos protocol (and ecosystem) evolves. If interested in proposing a change to this document, please feel free to submit an issue or make a merge request to [this Gitlab repository](https://gitlab.com/tezosagora/tezos-wiki).

---
# Development
To run this repository locally, you'll need [Gitbook](https://github.com/GitbookIO/gitbook/blob/master/docs/setup.md)

Install dependencies
```
npm install
```

Build project
```
npm run build
```

Run project locally
```
npm start
```

---
# Getting started
This wiki covers a variety of Tezos topics ranging from "Baking" to "Governance" to "Smart Contracts". It also includes practical Tezos resources like wallets and block explorers.

For a longer video explanation of the Tezos protocol, including its origins and inspirations, [**see this video**](https://www.youtube.com/embed/ftA7O04yxXg).

To get started with Tezos developer documentation, [**see here**](http://tezos.gitlab.io/).

To get started with Michelson smart contracts, check out the [**camlCase tutorial series**](https://gitlab.com/camlcase-dev/michelson-tutorial/tree/master/01) and the [**Michelson section**](http://tezos.gitlab.io/whitedoc/michelson.html) of the Nomadic Labs documentation.

For those with technical questions about Tezos, check out the [**Tezos Stack Exchange**](https://tezos.stackexchange.com/).

----

[ci]: https://about.gitlab.com/gitlab-ci/
[GitBook]: https://www.gitbook.com/
[host the book]: https://gitlab.com/pages/gitbook/tree/pages
[install]: http://toolchain.gitbook.com/setup.html
[documentation]: http://toolchain.gitbook.com
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
